<?php

use Phalcon\DI\FactoryDefault\CLI as CliDI;
use Phalcon\CLI\Console as ConsoleApp;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

/**
 * Include helper functions
 */
include __DIR__ . '/functions.php';

/**
 * Read the configuration
 */
$config = include __DIR__ . "/config/config.php";

/**
 * Read auto-loader
 */
include __DIR__ . "/config/loader.php";

 // using the CLI factory default services container
$di = new CliDI();

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    return new DbAdapter(array(
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset,
    ));
});

// create a console application
$console = new ConsoleApp();
$console->setDI($di);

// process the console arguments
$arguments = array();
foreach ($argv as $k => $arg) {
    if ($k == 1) {
        $arguments['task'] = $arg;
    } elseif ($k >= 2) {
        $arguments[] = $arg;
    }
}

// define global constants for the current task and action
define('CURRENT_TASK', (isset($arguments['task']) ? $arguments['task'] : null));
define('CURRENT_ACTION', (isset($arguments['action']) ? $arguments['action'] : null));

try {
    // handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit(255);
}
