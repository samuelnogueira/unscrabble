<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->tasksDir,
    )
)->register();

// Use autoloading with namespaces prefixes
$loader->registerNamespaces(
    array(
        'Unscrabble\Controllers' => $config->application->controllersDir,
        'Unscrabble\Models'      => $config->application->modelsDir,
        'Unscrabble\Tasks'       => $config->application->tasksDir,
    )
)->register();