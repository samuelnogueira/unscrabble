<?php

namespace Unscrabble\Controllers;

use Phalcon\Mvc\Controller;

/**
 * @author Samuel Nogueira <samuel.nogueira.dev@gmail.com>
 */
abstract class ControllerBase extends Controller
{
}
