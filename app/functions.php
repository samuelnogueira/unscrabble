<?php

/**
 * Converts a multi-byte string to an array
 * @param string $string The input string
 * @return string[]
 */
function mb_str_split($string)
{
    return preg_split('/(?<!^)(?!$)/u', $string);
}