<?php

namespace Unscrabble\Models\Dictionary\Reader;

class IspellIterator extends \Unscrabble\Models\Iterator\FileIterator
{
    /**
     * Return the current element
     *
     * Similar to the current() function for arrays.
     * Implement Iterator::current()
     *
     * @return  string
     * @see     Iterator::current()
     */
    public function current()
    {
        $current = trim(parent::current());
        $result  = preg_match('/^[^\/]*/', $current, $matches);
        
        if ($result === 1) {
            return $matches[0];
        }
    }
}