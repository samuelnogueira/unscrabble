<?php

namespace Unscrabble\Models\Dictionary\Reader;

use Unscrabble\Models\Dictionary\Reader\IspellIterator;

/**
 * Ispell dicionary reader
 * @author Samuel Nogueira <samuel.nogueira.dev@gmail.com>
 */
class IspellReader implements ReaderInterface
{
    /**
     * Returns the word string iterator for the supplied file
     * @param string $file Path to file
     * @return Iterator word string iterator
     */
    public function getStringIterator($file)
    {
        return new IspellIterator($file);
    }
}
