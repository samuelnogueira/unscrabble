<?php

namespace Unscrabble\Models\Dictionary\Reader;

/**
 * Dicionary reader interface
 * @author Samuel Nogueira <samuel.nogueira.dev@gmail.com>
 */
interface ReaderInterface
{
    /**
     * Returns the word string iterator for the supplied file
     * @param string $file Path to file
     * @return Iterator word string iterator
     */
    public function getStringIterator($file);
}
