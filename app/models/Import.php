<?php

namespace Unscrabble\Models;

use Unscrabble\Models\Language;
use Unscrabble\Models\Letter;
use Unscrabble\Models\Word;
use Transliterator;

/**
 * @author Samuel Nogueira <samuel.nogueira.dev@gmail.com>
 */
class Import
{
    /**
     * @var Language
     */
    protected $language = null;
    
    /**
     * @var int
     */
    protected $maxLength = 15;
    
    /**
     * @var int
     */
    protected $minLength = 2;
    
    /**
     * @var char[int]
     */
    protected $storedChars = null;
    
    /**
     * @var string[int]
     */
    protected $storedWords = null;
    
    /**
     * @var Transliterator
     */
    protected $transliterator = null;
    
    /**
     * @param Language $language
     */
    public function __construct(Language $language)
    {
        $this->language = $language;
    }
    
    /**
     * @param int $minLength
     */
    public function setMinLength($minLength)
    {
        if ($minLength < 0 || !is_numeric($minLength)) {
            throw new InvalidArgumentException(
                "Provided min length must be a positive integer. '$minLength' given"
            );
        }
        
        if ($minLength > $this->maxLength) {
            throw new InvalidArgumentException(
                "Min length ($minLength) cannot be greater than max length ({$this->maxLength})"
            );
        }
        
        $this->minLength = $minLength;
    }
    
    /**
     * @param int $maxLength
     */
    public function setMaxLength($maxLength)
    {
        if ($maxLength < 0 || !is_numeric($maxLength)) {
            throw new InvalidArgumentException(
                "Provided max length must be a positive integer. '$maxLength' given"
            );
        }
        
        if ($maxLength < $this->minLength) {
            throw new InvalidArgumentException(
                "Max length ($maxLength) cannot be smaller than min length ({$this->minLength})"
            );
        }
        
        $this->maxLength = (int) $maxLength;
    }
    
    /**
     * @param Transliterator $transliterator
     */
    public function setTransliterator(Transliterator $transliterator)
    {
        $this->transliterator = $transliterator;
    }
    
    /**
     * @param string $string
     * @return Word|false Returns the stored Word model instance if
     * successfull, false otherwise
     * @throws Exception if unable to store word
     */
    public function storeString($string)
    {
        $storedWords = $this->getStoredWords();
        $string      = $this->transliterate($string);
        $length      = mb_strlen($string);
        
        $isAlreadyStored = in_array($string, $storedWords);
        if ($length >= $this->minLength && $length <= $this->maxLength && !$isAlreadyStored) {
            
            // create words letters associations
            $wordHasLetters = array();
            foreach ($this->getLetterIds($string) as $i => $letterId) {
                $wordHasLetter = new WordHasLetter();
                $wordHasLetter->setFkLetter($letterId);
                $wordHasLetter->setPosition($i + 1);
                
                $wordHasLetters[] = $wordHasLetter;
            }
            
            $word = new Word();
            $word->setFkLanguage($this->language->getIdLanguage());
            $word->setString($string);
            $word->wordHasLetter = $wordHasLetters;
            
            if ($word->save() === false) {
                throw new Exception("Unable to store word: ".implode('; ', $word->getMessages()));
            }
            
            $this->storedWords[$word->getIdWord()] = $word->getString();
            
            return $word;
        }
        
        return false;
    }
    
    /**
     * @return string[int] Word string indexed by it's ID
     */
    protected function getStoredWords()
    {
        if ($this->storedWords === null) {
            $this->storedWords = array();
            foreach (Word::find(array('fk_language' => $this->language->getIdLanguage())) as $model) {
                /* @var $model Word */
                $this->storedWords[$model->getIdWord()] = $model->getString();
            }
        }
        
        return $this->storedWords;
    }
    
    /**
     * Returns an array of char's indexed by it's letter ID
     * @return char[int]
     */
    protected function getStoredChars()
    {
        if ($this->storedChars === null) {
            $this->storedChars = array();
            foreach (Letter::find(array('fk_language' => $this->language->getIdLanguage())) as $model) {
                /* @var $model Letter */
                $this->storedChars[$model->getIdLetter()] = $model->getChar();
            }
        }
        
        return $this->storedChars;
    }
    
    /**
     * @return Transliterator
     */
    protected function getTransliterator()
    {
        if ($this->transliterator === null)  {
            $id             = 'NFD; [:Nonspacing Mark:] Remove; NFC';
            $transliterator = Transliterator::create($id);
            
            $this->setTransliterator($transliterator);
        }
        
        return $this->transliterator;
    }
    
    /**
     * Transliterates a string to an accepted char-set for the
     * language
     * @param string $string
     * @return string
     */
    protected function transliterate($string)
    {
        $storedLetters  = $this->getStoredChars();
        $transliterator = $this->getTransliterator();
        
        $translitString = null;
        foreach (mb_str_split($string) as $char) {
            
            // fallback non-existing letters with their transliterated
            // counterpart
            if (!in_array($char, $storedLetters)) {
                $char = $transliterator->transliterate($char);
            }
            
            $char = mb_strtoupper($char);
            
            // if character doesn't exist in accepted characters list,
            // the entire word is discarded
            if (!in_array($char, $storedLetters)) {
                
                return null;
            }
            
            $translitString .= $char;
        }
        
        return $translitString;
    }
    
    /**
     * Returns an ordered array of letter ID's that represent the
     * given string, or NULL if not all characters from the string
     * can be represented by the list of accepted letters.
     * @param string $translitString
     * @return int[]|null
     */
    protected function getLetterIds($translitString)
    {
        $storedLetters  = $this->getStoredChars();
        
        $ids = array();
        foreach (mb_str_split($translitString) as $char) {
            
            $letterId = array_search($char, $storedLetters);
            if ($letterId === false) {
                
                return null;
            }
            
            $ids[] = $letterId;
        }
        
        return $ids;
    }
}