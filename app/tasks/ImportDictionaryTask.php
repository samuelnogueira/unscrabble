<?php

use Unscrabble\Models\Language;
use Unscrabble\Models\Dictionary\Reader\ReaderInterface;
use Unscrabble\Models\Import;
use Phalcon\CLI\Task;
use Phalcon\Mvc\Model;

/**
 * Import Dictionary CLI Task
 *
 * @author Samuel Nogueira <samuel.nogueira.dev@gmail.com>
 */
class ImportDictionaryTask extends Task
{
    /**
     * @param string $file
     * @param string $format
     * @param string $languageIdentifier
     */
    public function mainAction($file = null, $format = null, $languageIdentifier = null)
    {
        try {
            // validate file
            if (!is_file($file)) {
                throw new InvalidArgumentException("File '$file' does not exist");
            }
            
            // validate format
            $reader = $this->getReader($format);
            
            // validate language
            $language = Language::findFirst(array(
                "conditions" => "iso2 = ?1 OR iso3 = ?1 OR name = ?1",
                "bind"       => array(1 => $languageIdentifier)
            ));
            if ($language === false) {
                throw new InvalidArgumentException("Language '$languageIdentifier' could not be found");
            }
            
            // every seems ok, do import
            $this->doImport($file, $reader, $language);
            
        } catch (InvalidArgumentException $e) {
            
            echo "[!] {$e->getMessage()}\n";
            $this->helpAction();
            
        }
    }
    
    public function helpAction()
    {
        echo "Usage: import-dictionary FILE FORMAT LANGUAGE\n";
        echo "Supported formats:\n";
        echo "  - Ispell\n";
    }
    
    /**
     * @param string $file
     * @param ReaderInterface $reader
     * @param Language $language
     */
    protected function doImport($file, ReaderInterface $reader, Language $language)
    {
        echo "Importing words for ".$language->getName()." language\n";
        
        $importModel    = new Import($language);
        $stringIterator = $reader->getStringIterator($file);
        
        foreach ($stringIterator as $string) {
            $word = $importModel->storeString($string);
            
            if ($word !== false) {
                echo "[ > ]  $string\t->\t'{$word->getString()}'\n";
            } else {
                echo "[SKP]  $string\n";
            }
        }
        
        echo "\n\nFinished!";
    }
    
    /**
     * Factory to retrieve correct dictionary reader
     * @param string $format
     * @return ReaderInterface
     * @throws InvalidArgumentException If format is unrecognized
     */
    protected function getReader($format)
    {
        switch ($format) {
            case 'Ispell':
            case 'ispell':
                return new Unscrabble\Models\Dictionary\Reader\IspellReader();
            
            default:
                throw new InvalidArgumentException("Unrecognized format '$format'");
        }
    }
}