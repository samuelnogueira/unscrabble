<?php

use Phalcon\CLI\Task;

class MainTask extends Task
{
    public function mainAction()
    {
        echo "\nAvailable commands:\n\n";
        echo "  - import-dictionary [FILE] [LANG]\n";
        echo "\n";
    }
}
