<?php

use ContainerInteropDoctrine\EntityManagerFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Snogueira\Unscrabble\Game\Repository\LetterRepository;
use Snogueira\Unscrabble\Game\Repository\LetterRepositoryFactory;
use Zend\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Zend\ServiceManager\ServiceManager;

return new ServiceManager([
    'abstract_factories' => [
        ReflectionBasedAbstractFactory::class,
    ],
    'factories'          => [
        EntityManagerInterface::class => EntityManagerFactory::class,
        LetterRepository::class       => LetterRepositoryFactory::class,
    ],
    'services'           => [
        'config' => [
            'letters'  => [
                'file' => __DIR__ . '/../data/letters.json',
            ],
            'doctrine' => [
                'connection' => [
                    'orm_default' => [
                        'params' => [
                            'url' => 'mysql://root@127.0.0.1/unscrabble',
                        ],
                    ],
                ],
                'driver'     => [
                    'orm_default' => [
                        'class' => AnnotationDriver::class,
                        'cache' => 'array',
                        'paths' => [
                            __DIR__ . '/../src/Game/Entity',
                        ],
                    ],
                ],
            ],
        ],
    ],
]);
