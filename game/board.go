package game

const (
	Cells = 15
	CellTypeNN = 0
	CellTypeDL = 1
	CellTypeTL = 2
	CellTypeDW = 3
	CellTypeTW = 4
)

type Cell struct {
	Tile rune
	Type int8
}

type Board struct {
	Cells [Cells * Cells]Cell
}

func (b *Board) Get(x int, y int) Cell {
	return b.Cells[y * Cells + x]
}

func (b *Board) SetTile(x int, y int, r rune) {
	b.Cells[y * Cells + x].Tile = r
}

func (b *Board) String() string {
	s := ""
	for y := 0; y < Cells; y++ {
		for x := 0; x < Cells; x++ {
			r := b.Get(x, y).Tile
			if r > 0 {
				s += string(r)
			} else {
				s += " "
			}
			s += " "
		}
		s += "\n"
	}

	return s
}