package ocr

import (
	"image"
	"image/color"
	"bitbucket.org/samuelnogueira/unscrabble/game"
	"io/ioutil"
	"os"
	"math"
	"log"
)

type tileImg struct {
	img image.Image
	tile rune
}

const (
	tileWindowInset = 7
)

var (
	colorBkg = color.RGBA{
		R: uint8(199),
		G: uint8(217),
		B: uint8(230),
	}
	tileImgs []tileImg
)

func Parse(img image.Image) game.Board {
	board := game.Board{}
	bb := boardBounds(img)

	var col int
	var rect image.Rectangle
	for row := 0; row < game.Cells; row++ {
		for col = 0; col < game.Cells; col++ {
			rect = GetCellBounds(bb, col, row)
			board.SetTile(col, row, determineTile(img, rect))
		}
	}

	return board
}

func GetCellBounds(bb image.Rectangle, col, row int) image.Rectangle {
	cellW := bb.Dx() / game.Cells
	cellH := bb.Dy() / game.Cells

	minX := col * bb.Dx() / game.Cells + bb.Min.X
	minY := row * bb.Dy() / game.Cells + bb.Min.Y

	return image.Rect(minX, minY, minX + cellW, minY + cellH)
}

func getTileImages() []tileImg {
	if tileImgs == nil {
		tileImgs = make([]tileImg, 0)
		files, err := ioutil.ReadDir("cells")
		if err != nil {
			panic(err)
		}

		for _, fileInfo := range files {
			fileName := fileInfo.Name()
			file, err := os.Open("cells/" + fileName)
			if err != nil {
				panic(err)
			}
			img, _, err := image.Decode(file)
			file.Close()
			if err != nil {
				panic(err)
			}
			
			tileImgs = append(tileImgs, tileImg{
				img: img,
				tile: rune(fileName[0]),
			})
		}
	}

	return tileImgs
}

func determineTile(img image.Image, win image.Rectangle) rune {
	var min int64 = 900000
	var tile rune
	var tileBounds image.Rectangle

	win = win.Inset(tileWindowInset)

	for _, tileImg := range getTileImages() {
		tileBounds = tileImg.img.Bounds().Inset(tileWindowInset)

		d := imageWindowsCompare(img, win, tileImg.img, tileBounds)
		if d < min {
			tile = tileImg.tile
			min  = d
		}
	}

	log.Println(string(tile), min)

	return tile
}

func boardBounds(img image.Image) image.Rectangle {
	minY := 0
	maxX := img.Bounds().Max.X
	maxY := img.Bounds().Max.Y

	var x int
	for y := 0; y < maxY; y++ {
		for x = 0; x < maxX; x++ {
			if !rgbEquals(img.At(x, y), colorBkg) {
				break
			}
		}

		// entire line is background color
		if x == maxX {
			minY = y + 1
		} else if minY > 0 {
			break
		}
	}

	return image.Rectangle{
		Min: image.Point{
			X: 0,
			Y: minY,
		},
		Max: image.Point{
			X: maxX,
			Y: minY + maxX,
		},
	}
}

func rgbEquals(c1 color.Color, c2 color.Color) bool {
	r1, g1, b1, _ := c1.RGBA()
	r2, g2, b2, _ := c2.RGBA()

	return r1 == r2 && g1 == g2 && b1 == b2
}

func imageWindowsCompare(img1 image.Image, win1 image.Rectangle, img2 image.Image, win2 image.Rectangle) int64 {
	if !win1.Size().Eq(win2.Size()) {
		panic("different sizes")
	}

	sumError := int64(0)
	var x, y, x1, x2, y1, y2 int
	for x = win1.Dx(); x >= 0; x-- {
		x1 = win1.Min.X + x
		x2 = win2.Min.X + x

		for y = win1.Dy(); y >= 0; y-- {
			y1 = win1.Min.Y + y
			y2 = win2.Min.Y + y

			sumError += colorDiffSquared(img1.At(x1, y1), img2.At(x2, y2))
		}
	}

	return int64(math.Sqrt(float64(sumError)))
}

func colorDiffSquared(c1 color.Color, c2 color.Color) int64 {
	r1, g1, b1, _ := c1.RGBA()
	r2, g2, b2, _ := c2.RGBA()

	dR := uint64(r2) - uint64(r1)
	dG := uint64(g2) - uint64(g1)
	dB := uint64(b2) - uint64(b1)

	return int64(dR * dR + dG * dG + dB * dB)
}