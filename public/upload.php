<?php

use Snogueira\Unscrabble\ImageManipulation\Imagick\Image;
use Snogueira\Unscrabble\OCR\Tesseract\TesseractRecognizer;
use Snogueira\Unscrabble\OpticalRecognition\OpticalBoardRecognizer;

require __DIR__ . '/../vendor/autoload.php';

(static function () {
    $image      = Image::fromFilename($_FILES['pic']['tmp_name']);
    $recognizer = new OpticalBoardRecognizer($image, new TesseractRecognizer());
    $board      = $recognizer->extractBoard();

    var_dump($board);
})();
