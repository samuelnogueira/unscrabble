<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\BufferedOutput;
use Webmozart\Assert\Assert;
use function array_merge;
use function array_slice;
use function range;

final class Board
{
    /** @var int */
    private $rows;
    /** @var int */
    private $cols;
    /** @var Tile[][] */
    private $tiles = [];

    public function __construct(int $rows, int $cols)
    {
        Assert::greaterThan($rows, 0);
        Assert::greaterThan($cols, 0);

        $this->rows = $rows;
        $this->cols = $cols;
    }

    public function setTile(MatrixPosition $position, Tile $tile): void
    {
        $this->tiles[$position->getRow()][$position->getCol()] = $tile;
    }

    public function __toString(): string
    {
        $output  = new BufferedOutput();
        $table   = new Table($output);
        $headers = array_merge([''], array_slice(range('A', 'Z'), 0, $this->cols));
        $table->setHeaders($headers);

        for ($row = 1; $row <= $this->rows; $row++) {
            $tableRow = [$row];
            for ($col = 1; $col <= $this->cols; $col++) {
                $tile       = $this->tileAt($row, $col);
                $tableRow[] = $tile !== null ? $tile->getLabel() : '?';
            }
            $table->addRow($tableRow);
        }
        $table->render();

        return $output->fetch();
    }

    private function tileAt(int $row, int $col): ?Tile
    {
        return $this->tiles[$row][$col] ?? null;
    }
}
