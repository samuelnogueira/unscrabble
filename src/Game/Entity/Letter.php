<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game\Entity;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity()
 */
final class Letter
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="`character`", unique=true, length=1)
     */
    private $character;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $points;

    public function __construct(string $character, int $points)
    {
        Assert::length($character, 1);
        Assert::greaterThanEq($points, 1);

        $this->id        = null;
        $this->character = $character;
        $this->points    = $points;
    }
}
