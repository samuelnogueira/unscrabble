<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Snogueira\Unscrabble\Game\Repository\LetterRepository;
use function strlen;

/**
 * @ORM\Entity()
 */
final class Word
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Collection|WordLetter[]
     * @ORM\OneToMany(
     *     targetEntity="\Snogueira\Unscrabble\Game\Entity\WordLetter",
     *     mappedBy="word",
     *     cascade={"PERSIST"}
     * )
     */
    private $wordLetters;

    /**
     * @var string
     * @ORM\Column(unique=true)
     */
    private $string;

    public function __construct(string $string, LetterRepository $letterRepository)
    {
        $this->id          = null;
        $this->wordLetters = new ArrayCollection();
        $this->string      = $string;

        for ($index = 0; $index < strlen($string); $index++) {
            $char       = $string[$index];
            $letter     = $letterRepository->findOrCreateByCharacter($char);
            $wordLetter = new WordLetter($this, $letter, $index);

            $this->wordLetters->add($wordLetter);
        }
    }
}
