<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     uniqueConstraints={@UniqueConstraint(columns={"index", "word_id"})}
 * )
 */
final class WordLetter
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Word
     * @ORM\ManyToOne(targetEntity="\Snogueira\Unscrabble\Game\Entity\Word")
     * @ORM\JoinColumn(nullable=false)
     */
    private $word;

    /**
     * @var Letter
     * @ORM\ManyToOne(targetEntity="\Snogueira\Unscrabble\Game\Entity\Letter")
     * @ORM\JoinColumn(nullable=false)
     */
    private $letter;

    /**
     * @var int
     * @ORM\Column(name="`index`", type="smallint")
     */
    private $index;

    public function __construct(Word $word, Letter $letter, int $index)
    {
        $this->id     = null;
        $this->word   = $word;
        $this->letter = $letter;
        $this->index  = $index;
    }
}
