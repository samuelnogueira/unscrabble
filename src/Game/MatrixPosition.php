<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game;

use Webmozart\Assert\Assert;

final class MatrixPosition
{
    /** @var int */
    private $row;
    /** @var int */
    private $col;

    public function __construct(int $row, int $col)
    {
        Assert::greaterThan($row, 0);
        Assert::greaterThan($col, 0);

        $this->row = $row;
        $this->col = $col;
    }

    public function getRow(): int
    {
        return $this->row;
    }

    public function getCol(): int
    {
        return $this->col;
    }

    public function getColHeader(): string
    {
        return range('A', 'Z')[($this->col - 1) % 26];
    }

    public function getLabel(): string
    {
        return "{$this->getColHeader()}-{$this->row}";
    }
}
