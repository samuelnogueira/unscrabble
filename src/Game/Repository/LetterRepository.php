<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Snogueira\Unscrabble\Game\Entity\Letter;

final class LetterRepository
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var Letter[] */
    private $lettersByChar = [];
    /** @var object */
    private $letterValues;

    public function __construct(EntityManagerInterface $em, object $letterValues)
    {
        $this->em           = $em;
        $this->letterValues = $letterValues;
    }

    public function findOrCreateByCharacter(string $character): Letter
    {
        if (! isset($this->lettersByChar[$character])) {
            $letter = $this->em->getRepository(Letter::class)->findOneBy([
                'character' => $character,
            ]);
            if ($letter === null) {
                $letter = new Letter($character, $this->letterValues->$character);
                $this->em->persist($letter);
            }

            $this->lettersByChar[$character] = $letter;
        }

        return $this->lettersByChar[$character];
    }
}
