<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Safe\Exceptions\FilesystemException;
use Safe\Exceptions\JsonException;

final class LetterRepositoryFactory
{
    /**
     * @throws FilesystemException
     * @throws JsonException
     */
    public function __invoke(ContainerInterface $container): LetterRepository
    {
        return new LetterRepository(
            $container->get(EntityManagerInterface::class),
            \Safe\json_decode(\Safe\file_get_contents($container->get('config')['letters']['file']))
        );
    }
}
