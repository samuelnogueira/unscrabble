<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Snogueira\Unscrabble\Game\Entity\Word;
use function array_column;
use function array_diff;

final class WordRepository
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function add(Word $word): void
    {
        $this->em->persist($word);
    }

    public function count(): int
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return (int) $this->em->createQuery(<<<DQL
SELECT COUNT(1)
  FROM Snogueira\Unscrabble\Game\Entity\Word w
DQL
        )
            ->getSingleScalarResult();
    }

    /**
     * @param string[] $strings
     *
     * @return string[]
     */
    public function filterExisting(array $strings): array
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $existingStrings = $this->em->createQuery(<<<DQL
SELECT w.string
  FROM Snogueira\Unscrabble\Game\Entity\Word w
 WHERE w.string IN (:strings)
DQL
        )
            ->setParameter('strings', $strings)
            ->getScalarResult();

        $existingStrings = array_column($existingStrings, 'string');

        return array_diff($strings, $existingStrings);
    }
}
