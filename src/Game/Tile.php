<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Game;

final class Tile
{
    /** @var string */
    private $label;

    public function __construct(string $label)
    {
        $this->label = $label;
    }

    public static function vacant(): self
    {
        return new self(' ');
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
