<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\Helper;

use function dirname;

final class Dir
{
    public static function root(?string $path = null): string
    {
        $filename = dirname(__DIR__) . '/../';
        if ($path !== null) {
            $filename .= "/{$path}";
        }

        return $filename;
    }

    public static function data(?string $path = null): string
    {
        $filename = dirname(__DIR__) . '/../data';
        if ($path !== null) {
            $filename .= "/{$path}";
        }

        return $filename;
    }
}
