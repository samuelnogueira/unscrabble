<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\ImageManipulation;

final class Color
{
    /** @var int */
    private $red;
    /** @var int */
    private $green;
    /** @var int */
    private $blue;

    public function __construct(int $red, int $green, int $blue)
    {
        $this->red   = $red;
        $this->green = $green;
        $this->blue  = $blue;
    }

    public function isWhite(): bool
    {
        return $this->red === 255
            && $this->green === 255
            && $this->blue === 255;
    }

    public function equals(Color $color): bool
    {
        return $this->red === $color->red
            && $this->green === $color->green
            && $this->blue === $color->blue;
    }
}
