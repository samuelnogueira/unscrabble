<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\ImageManipulation;

interface ImageInterface
{
    /**
     * Returns the height in pixels of the image.
     */
    public function height(): int;

    /**
     * Returns the width in pixels of the image.
     */
    public function width(): int;

    /**
     * Returns a new image cut out from a rectangular part of the current image with given width and height with top
     * left corner at x, y point.
     */
    public function crop(int $width, int $height, int $x, int $y): ImageInterface;

    /**
     * Save the image object in filesystem.
     */
    public function save(string $filename): void;

    /**
     * Pick a color at point x, y out of image.
     */
    public function pickColor(Point $point): Color;

    /**
     * Returns a new image with all colors of the current image reversed.
     */
    public function invert(): ImageInterface;

    /**
     * Remove edges that are the background color from the image.
     */
    public function trim(): ImageInterface;

    /**
     * Returns a new image with contrast changed by the given level.
     * Use values between -100 for min. contrast 0 for no change and +100 for max. contrast.
     */
    public function contrast(int $level): ImageInterface;
}
