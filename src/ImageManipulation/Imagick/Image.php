<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\ImageManipulation\Imagick;

use Imagick;
use ImagickException;
use ImagickPixelException;
use Snogueira\Unscrabble\ImageManipulation\Color;
use Snogueira\Unscrabble\ImageManipulation\ImageInterface;
use Snogueira\Unscrabble\ImageManipulation\Point;

final class Image implements ImageInterface
{
    /** @var Imagick */
    private $imagick;

    public function __construct(Imagick $imagick)
    {
        $this->imagick = $imagick;
    }

    /**
     * @throws ImagickException
     */
    public static function fromFilename(string $filename): self
    {
        return new self(new Imagick($filename));
    }

    public function crop(int $width, int $height, int $x, int $y): ImageInterface
    {
        $imagick = $this->imagick->getImageRegion($width, $height, $x, $y);
        self::resetImagePage($imagick);

        return new self($imagick);
    }

    public function height(): int
    {
        return $this->imagick->getImageHeight();
    }

    public function invert(): ImageInterface
    {
        $imagick = $this->imagick->clone();
        if (! $imagick->negateImage(false)) {
            throw ImagickRuntimeException::invertFailed();
        }

        return new self($imagick);
    }

    /**
     * @throws ImagickPixelException
     */
    public function pickColor(Point $point): Color
    {
        ['r' => $r, 'g' => $g, 'b' => $b] = $this->imagick
            ->getImagePixelColor($point->x(), $point->y())
            ->getColor();

        return new Color($r, $g, $b);
    }

    public function save(string $filename): void
    {
        if (! $this->imagick->writeImage($filename)) {
            throw ImagickRuntimeException::saveFailed($filename);
        }
    }

    public function width(): int
    {
        return $this->imagick->getImageWidth();
    }

    public function trim(): ImageInterface
    {
        $imagick = $this->imagick->clone();
        if (! $imagick->trimImage(0.0)) {
            throw ImagickRuntimeException::trimFailed();
        }
        self::resetImagePage($imagick);

        return new self($imagick);
    }

    public function contrast(int $level): ImageInterface
    {
        $sharpen = $level > 0;
        $alpha   = $level / 4;
        $imagick = $this->imagick->clone();
        if (! $imagick->sigmoidalContrastImage($sharpen, $alpha, 0)) {
            throw ImagickRuntimeException::contrastFailed();
        }

        return new self($imagick);
    }

    private static function resetImagePage(Imagick $imagick): void
    {
        if (! $imagick->setImagePage(0, 0, 0, 0)) {
            throw ImagickRuntimeException::resetPageFailed();
        };
    }
}
