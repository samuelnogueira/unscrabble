<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\ImageManipulation\Imagick;

use RuntimeException;

final class ImagickRuntimeException extends RuntimeException
{
    public static function invertFailed(): self
    {
        return new self('Could not negate image colors');
    }

    public static function saveFailed(string $filename): self
    {
        return new self("Could not write image to file '$filename'");
    }

    public static function trimFailed(): self
    {
        return new self("Could not trim image");
    }

    public static function resetPageFailed(): self
    {
        return new self("Could not reset image page");
    }

    public static function contrastFailed(): self
    {
        return new self("Could not change image contrast");
    }
}
