<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\ImageManipulation;

use Webmozart\Assert\Assert;

final class Point
{
    /** @var int */
    private $x;
    /** @var int */
    private $y;

    public function __construct(int $x, int $y)
    {
        Assert::natural($x);
        Assert::natural($y);

        $this->x = $x;
        $this->y = $y;
    }

    public static function atCenterOf(ImageInterface $image): self
    {
        return new self(
            (int) ($image->width() / 2),
            (int) ($image->height() / 2)
        );
    }

    public function x(): int
    {
        return $this->x;
    }

    public function y(): int
    {
        return $this->y;
    }
}
