<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\OCR;

use Snogueira\Unscrabble\ImageManipulation\ImageInterface;

interface RecognizerInterface
{
    public function readCharacter(ImageInterface $image): string;
}
