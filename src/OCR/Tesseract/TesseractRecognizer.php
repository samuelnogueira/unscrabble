<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\OCR\Tesseract;

use Safe\Exceptions\FilesystemException;
use Snogueira\Unscrabble\ImageManipulation\ImageInterface;
use Snogueira\Unscrabble\OCR\RecognizerInterface;
use thiagoalessio\TesseractOCR\TesseractOCR;
use function sys_get_temp_dir;

final class TesseractRecognizer implements RecognizerInterface
{
    /**
     * @throws FilesystemException
     */
    public function readCharacter(ImageInterface $image): string
    {
        $filename = \Safe\tempnam(sys_get_temp_dir(), 'tesseract') . '.png';
        $image->save($filename);

        /** @noinspection PhpUndefinedMethodInspection */
        $character = (new TesseractOCR($filename))
            ->psm(10)
            ->run();

        \Safe\rename($filename, sys_get_temp_dir() . "/ocr-$character.png");

        return $character;
    }
}
