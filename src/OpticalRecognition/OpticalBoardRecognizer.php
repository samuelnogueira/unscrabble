<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\OpticalRecognition;

use ImagickPixel;
use Iterator;
use Snogueira\Unscrabble\Game\Board;
use Snogueira\Unscrabble\Game\MatrixPosition;
use Snogueira\Unscrabble\Helper\Dir;
use Snogueira\Unscrabble\ImageManipulation\ImageInterface;
use Snogueira\Unscrabble\OCR\RecognizerInterface;

final class OpticalBoardRecognizer
{
    /** @var int */
    private $cols = 15;
    /** @var int */
    private $rows = 15;
    /** @var ImagickPixel */
    private $tileBackground;
    /** @var ImageInterface */
    private $image;
    /** @var ImageInterface */
    private $boardImage;
    /** @var Board */
    private $board;
    /** @var float */
    private $tileWidth;
    /** @var float */
    private $tileHeight;
    /** @var RecognizerInterface */
    private $ocr;

    public function __construct(ImageInterface $image, RecognizerInterface $ocr)
    {
        $this->tileBackground = new ImagickPixel('srgb(238,236,233)');
        $this->image          = $image;
        $this->boardImage     = self::cropBoard($image);
        $this->board          = new Board($this->cols, $this->rows);
        $this->tileWidth      = $this->boardImage->width() / $this->cols;
        $this->tileHeight     = $this->boardImage->height() / $this->rows;
        $this->ocr            = $ocr;
    }

    public function extractBoard(): Board
    {
        $this->chopTiles();

        return $this->board;
    }

    private function chopTiles(): void
    {
        foreach ($this->getMatrixPositionsIterator() as $position) {
            $tileImage = $this->extractTileImage($position);
            $tileImage->save(Dir::data("{$position->getLabel()}.png"));

            $this->board->setTile(
                $position,
                (new OpticalTileRecognizer($tileImage))->readTile($this->ocr)
            );
        }
    }

    private function extractTileImage(MatrixPosition $position): ImageInterface
    {
        return $this->boardImage->crop(
            (int) $this->tileWidth,
            (int) $this->tileHeight,
            (int) ($this->tileWidth * ($position->getCol() - 1)),
            (int) ($this->tileHeight * ($position->getRow() - 1))
        );
    }

    private static function cropBoard(ImageInterface $image): ImageInterface
    {
        return $image->crop(1024, 1024, 27, 399);

        //        // Find a white line
        //        for ($y = $imageHeight / 2; $y >= 0; $y--) {
        //            if ($image->getImagePixelColor($imageWidth/ 2, $y)->getColorAsString() === $this->whiteColor) {
        //                break;
        //            }
        //        }
        //
        //        // Find where the white line starts
        //        for ($x = 0; $x < $imageWidth; $x++) {
        //            if ($image->getImagePixelColor($x, $y)->getColorAsString() === $this->whiteColor) {
        //                $boardX    = $x;
        //                $boardSize = $imageWidth - $x * 2;
        //                break;
        //            }
        //        }
        //
        //        for ($y = 0; $y < $imageHeight; $y++) {
        //            if ($this->isEntireLineBackgroundColor($image, $y)) {
        //                if ($y < $imageHeight / 2) {
        //                    $boardY = $y;
        //                } else {
        //                    $boardSize = $y - $boardY;
        //                }
        //            }
        //        }
        //
        //
        //        $image->cropImage($boardSize, $boardSize, $boardX, $boardY);
    }


    /**
     * @return Iterator|MatrixPosition[]
     */
    private function getMatrixPositionsIterator(): Iterator
    {
        for ($row = 1; $row <= $this->rows; $row++) {
            for ($col = 1; $col <= $this->cols; $col++) {
                yield new MatrixPosition($row, $col);
            }
        }
    }
}
