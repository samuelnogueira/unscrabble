<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\OpticalRecognition;

use Snogueira\Unscrabble\Game\Tile;
use Snogueira\Unscrabble\ImageManipulation\Color;
use Snogueira\Unscrabble\ImageManipulation\ImageInterface;
use Snogueira\Unscrabble\ImageManipulation\Point;
use Snogueira\Unscrabble\OCR\RecognizerInterface;

final class OpticalTileRecognizer
{
    /** @var ImageInterface */
    private $image;

    public function __construct(ImageInterface $image)
    {
        $this->image = $image;
    }

    private static function backgroundColor(): Color
    {
        return new Color(238, 236, 233);
    }

    public function readTile(RecognizerInterface $ocr): Tile
    {
        if ($this->isVacant()) {
            return Tile::vacant();
        }

        $image = $this->hasWhiteLetters()
            ? $this->image->invert()
            : $this->image;
        $image = $image
            ->trim()
            ->crop($image->width() - 12, $image->height() - 12, 0, 12)
            ->contrast(50);

        $label = $ocr->readCharacter($image);

        return new Tile($label);
    }

    private function hasWhiteLetters(): bool
    {
        // Trace a line spanning half the width of the image, centered.
        $y      = (int) ($this->image->height() / 2);
        $width  = (int) ($this->image->width() / 2);
        $offset = (int) ($this->image->width() / 4);
        for ($i = 0; $i < $width; $i++) {
            $x = $offset + $i;
            if ($this->image->pickColor(new Point($x, $y))->isWhite()) {
                return true;
            }
        }

        return false;
    }

    private function isVacant(): bool
    {
        return $this->image
            ->pickColor(Point::atCenterOf($this->image))
            ->equals(self::backgroundColor());
    }
}
