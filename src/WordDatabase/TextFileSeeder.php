<?php

declare(strict_types=1);

namespace Snogueira\Unscrabble\WordDatabase;

use Doctrine\ORM\EntityManagerInterface;
use Safe\Exceptions\FilesystemException;
use Snogueira\Unscrabble\Game\Entity\Word;
use Snogueira\Unscrabble\Game\Repository\LetterRepository;
use Snogueira\Unscrabble\Game\Repository\WordRepository;
use function array_chunk;
use function array_map;

final class TextFileSeeder
{
    /** @var WordRepository */
    private $wordRepository;
    /** @var LetterRepository */
    private $letterRepository;
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        WordRepository $wordRepository,
        LetterRepository $letterRepository,
        EntityManagerInterface $em
    ) {
        $this->wordRepository   = $wordRepository;
        $this->letterRepository = $letterRepository;
        $this->em               = $em;
    }

    /**
     * @throws FilesystemException
     */
    public function seed(string $filename): void
    {
        $lines = \Safe\file($filename);
        if ($this->wordRepository->count() === count($lines)) {
            return;
        }
        $strings = array_map('trim', $lines);
        foreach (array_chunk($strings, 1000) as $stringsChunk) {
            $missingStrings = $this->wordRepository->filterExisting($stringsChunk);
            foreach ($missingStrings as $string) {
                $this->wordRepository->add(new Word($string, $this->letterRepository));
            }
            $this->em->flush();
        }
    }
}
