<?php

declare(strict_types=1);

namespace Snogueira\UnscrabbleTests\Lib;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

abstract class AppTestCase extends TestCase
{
    /** @var ContainerInterface|null */
    private static $container;

    protected function getContainer(): ContainerInterface
    {
        if (self::$container === null) {
            self::$container = require __DIR__ . '/../../config/container.php';
        }

        return self::$container;
    }
}
