<?php

declare(strict_types=1);

namespace Snogueira\UnscrabbleTests\OpticalRecognition;

use PHPUnit\Framework\TestCase;
use Snogueira\Unscrabble\ImageManipulation\Imagick\Image;
use Snogueira\Unscrabble\OCR\Tesseract\TesseractRecognizer;
use Snogueira\Unscrabble\OpticalRecognition\OpticalBoardRecognizer;

final class OpticalBoardRecognizerTest extends TestCase
{
    public function testRecognize(): void
    {
        $image   = Image::fromFilename(__DIR__ . '/fixtures/Screenshot_B.png');
        $subject = new OpticalBoardRecognizer($image, new TesseractRecognizer());

        echo $subject->extractBoard();

        static::assertTrue(true);
    }
}
