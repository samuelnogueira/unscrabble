<?php

declare(strict_types=1);

namespace Snogueira\UnscrabbleTests\WordDatabase;

use Snogueira\Unscrabble\WordDatabase\TextFileSeeder;
use Snogueira\UnscrabbleTests\Lib\AppTestCase;

final class TextFileSeederTest extends AppTestCase
{
    public function testSeed(): void
    {
        $container = $this->getContainer();

        $seeder = $container->get(TextFileSeeder::class);
        $seeder->seed(__DIR__ . '/../../data/sowpods.txt');
    }
}
