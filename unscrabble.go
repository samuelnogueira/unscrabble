package main

import (
	"net/http"
	"html/template"
	"image"
	"fmt"
	_ "image/jpeg"
	_ "image/png"
	_ "image/gif"
	"bitbucket.org/samuelnogueira/unscrabble/ocr"
)

func errorHandler(w http.ResponseWriter, err error) {
	w.WriteHeader(503)
	fmt.Fprint(w, err)
}

// upload logic
func upload(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		t, err := template.ParseFiles("templates/upload.html")
		if err != nil {
			panic(err)
		}
		t.Execute(w, nil)
	} else {
		r.ParseMultipartForm(32 << 20)
		file, _, err := r.FormFile("pic")
		if err != nil {
			errorHandler(w, err)
			return
		}
		defer file.Close()

		img, _, err := image.Decode(file)
		if err != nil {
			errorHandler(w, err)
			return
		}

		board := ocr.Parse(img)
		fmt.Println(board.String())
	}
}


// sample usage
func main() {
	http.HandleFunc("/", upload)
	http.ListenAndServe(":9090", nil)
}